﻿
using System;
using NUnit.Framework;
using XamarinNotes.Model;
using GalaSoft.MvvmLight.Views;

namespace XamarinNotes.Tests
{
	[TestFixture]
	public class NoteTest
	{
		[Test]
		public void Note_NoteIsCreated_DateStringReturnsExpectedValue ()
		{
			var note = new Note ();
			var expectedDateString = DateTime.Today.ToString("D");
			Assert.True (note.DateString.Length > 0);
			Assert.AreEqual (note.DateString, expectedDateString);
		}
	}
}
