﻿using System;
using GalaSoft.MvvmLight;

namespace XamarinNotes.Model
{
	public class Note : ObservableObject
	{
		private string text;
		public DateTime Date {
			get;
			set;
		}

		public Note ()
		{
			this.Date = DateTime.Now;
		}

		public string Text {
			get { return this.text; }
			set { this.Set (() => Text, ref text, value); }
		}

		public string DateString {
			get { return this.Date.ToString ("D"); }
		}
	}
}

