﻿using Android.App;
using Android.Widget;
using Android.OS;
using XamarinNotes.ViewModel;
using XamarinNotes.Model;
using Android.Views;
using GalaSoft.MvvmLight.Helpers;

namespace XamarinNotes.Droid
{
	[Activity (Label = "Notes", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private ListView noteList;
		private Button addButton;
		public ListView NoteList
		{
			get
			{
				return noteList
					?? (noteList = FindViewById<ListView>(Resource.Id.NotesList));
			}
		}

		public Button AddButton
		{
			get
			{
				return addButton
					?? (addButton = FindViewById<Button>(Resource.Id.AddButton));
			}
		}

		int count = 1;

		private NoteListViewModel listViewModel; 

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.NoteListView);

			this.listViewModel = new NoteListViewModel ();

			// TODO Remove Mock Data
			var note1 = new Note ();
			note1.Text = "Die erste Notiz";
			var note2 = new Note ();
			note2.Text = "Die zweite Notiz";
			var note3 = new Note ();
			note3.Text = "Die dritte Notiz";

			this.listViewModel.List.Add (new NoteViewModel(note1));
			this.listViewModel.List.Add (new NoteViewModel(note2));
			this.listViewModel.List.Add (new NoteViewModel(note3));

			this.NoteList.Adapter = this.listViewModel.List.GetAdapter(GetNoteAdapter);
			AddButton.Click += (s, e) =>
			{
				StartActivity(typeof(NoteActivity));
			};

			AddButton.SetCommand(
				"Click",
				this.listViewModel.AddCommand);

		}

		private View GetNoteAdapter(int position, NoteViewModel viewModel, View convertView)
		{
			// Not reusing views here
			convertView = LayoutInflater.Inflate(Resource.Layout.NoteTemplate, null);

			var title = convertView.FindViewById<TextView>(Resource.Id.NameTextView);
			title.Text = viewModel.Note.Text;

			var desc = convertView.FindViewById<TextView>(Resource.Id.DescriptionTextView);
			desc.Text = viewModel.Note.Date;

			return convertView;
		}


	}
}


