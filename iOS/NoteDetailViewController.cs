using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using XamarinNotes.ViewModel;
using GalaSoft.MvvmLight.Helpers;

namespace XamarinNotes.iOS
{
	partial class NoteDetailViewController : UIViewController
	{

		public NoteViewModel noteViewModel {
			get;
			set;
		}

		public NoteDetailViewController (IntPtr handle) : base (handle) {}

		public override void ViewDidLoad () {
			base.ViewDidLoad ();

			// Set binding of model to view
			this.SetBinding(() => this.noteViewModel.Note.Text).WhenSourceChanges(() => {
				this.noteTextView.Text = this.noteViewModel.Note.Text;
			});

			// Set binding of view to model
			this.SetBinding (() => this.noteTextView.Text).UpdateSourceTrigger ("Changed")
				.WhenSourceChanges (() => this.noteViewModel.Note.Text = this.noteTextView.Text);

			this.Title = "Notiz vom " + this.noteViewModel.Note.DateString;
				
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			if (this.noteTextView.Text.Length == 0) {
				this.noteTextView.BecomeFirstResponder ();
			}
		}

		public override void ViewWillDisappear (bool animated)
		{
			this.noteViewModel.Save ();
			base.ViewWillDisappear (animated);
		}
	}
}
