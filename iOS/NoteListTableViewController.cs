using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using XamarinNotes.ViewModel;
using GalaSoft.MvvmLight.Helpers;
using XamarinNotes.Model;
using System.Linq;


namespace XamarinNotes.iOS
{
	partial class NoteListTableViewController : UIViewController
	{
		private const float RowHeight = 50;
		private const string CreateNoteSegue = "createNoteSegue";
		private const string EditNoteSegue = "editNoteSegue";

		private ObservableTableViewController<NoteViewModel> tableViewController;
		private NoteListViewModel listViewModel; 

		public NoteListTableViewController (IntPtr handle) : base (handle)
		{
			this.listViewModel = new NoteListViewModel ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.tableViewController = this.listViewModel.List.GetController (CreateNoteCell, BindNoteCell);
			this.tableViewController.TableView = this.NotesTableView;
			this.tableViewController.TableView.RowHeight = RowHeight;
			this.tableViewController.TableView.WeakDelegate = this;
		}

		public override void ViewWillAppear (bool animated) {
			base.ViewWillAppear (animated);
			this.tableViewController.TableView.ReloadData ();
		}

		private void BindNoteCell(UITableViewCell cell, NoteViewModel noteViewModel, NSIndexPath path)
		{
			cell.TextLabel.Text = noteViewModel.Note.Text;
			cell.DetailTextLabel.Text = noteViewModel.Note.DateString;
		}

		private UITableViewCell CreateNoteCell(NSString reuseId)
		{
			var cell = new UITableViewCell(UITableViewCellStyle.Subtitle, null);
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			return cell;
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender) 
		{
			if (segue.Identifier == CreateNoteSegue) {
				this.listViewModel.Add ();
				NoteDetailViewController detailViewController = (NoteDetailViewController)segue.DestinationViewController;
				detailViewController.noteViewModel = this.listViewModel.SelectedViewModel;

			} else if (segue.Identifier == EditNoteSegue) {
				NoteDetailViewController detailViewController = (NoteDetailViewController)segue.DestinationViewController;
				int row = this.NotesTableView.IndexPathForSelectedRow.Row;
				detailViewController.noteViewModel = this.listViewModel.List [row];
			}
		}

		[Export("tableView:didSelectRowAtIndexPath:")]
		public void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			this.PerformSegue (EditNoteSegue, null);
		}

	}
}
