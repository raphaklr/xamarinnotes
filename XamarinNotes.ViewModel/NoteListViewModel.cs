﻿using System;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using XamarinNotes.Model;
using System.Linq;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;

namespace XamarinNotes.ViewModel
{
	public class NoteListViewModel : ViewModelBase
	{
		private RelayCommand addCommand;
		// TODO Add Commands
		private RelayCommand editCommand;
		private RelayCommand removeCommand;

		public ObservableCollection<NoteViewModel> List {
			get;
			private set;
		}

		public NoteViewModel SelectedViewModel {
			get;
			private set;
		}

		public NoteListViewModel ()
		{
			this.List = new ObservableCollection<NoteViewModel> ();
		}

		public RelayCommand AddCommand {
			get { return addCommand = new RelayCommand (() => this.Add ()); }
		}

		public void Add ()
		{
			var newNote = new Note ();
			this.SelectedViewModel = new NoteViewModel (newNote);

			this.SelectedViewModel.Saved += this.OnSelectedViewModelSaved;
		}

		private void OnSelectedViewModelSaved (object sender, EventArgs e)
		{
			if (!this.List.Contains (this.SelectedViewModel)) {
				this.List.Insert (0, this.SelectedViewModel);
			}
		}
	}
}

