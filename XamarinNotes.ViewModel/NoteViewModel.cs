﻿using System;
using XamarinNotes.Model;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight;
using System.Xml.Linq;

namespace XamarinNotes.ViewModel 
{
	// TODO Add Command for Open View
	public class NoteViewModel : ObservableObject
	{
		private Note note;
		private RelayCommand saveCommand;
		public event EventHandler Saved;

		public NoteViewModel ()
		{
		}

		public NoteViewModel (Note note)
		{
			this.note = note;
		}

		public Note Note 
		{
			get { return this.note; }
			set { this.Set (() => Note, ref note, value); }
		}

		public RelayCommand SaveCommand 
		{
			get { return saveCommand =  new RelayCommand(() => this.Save()); }
		}

		public void Save() 
		{
			if (this.Saved == null) {
				return;
			}
			
			this.Saved (this, new EventArgs ());
		}
	}
}

